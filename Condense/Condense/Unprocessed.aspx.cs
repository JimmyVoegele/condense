﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Condense
{
    public partial class AllStudents : System.Web.UI.Page
    {
        //static string m_strStartTime = "";
        //static string m_strEndTime = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(Request.QueryString["Password"]))
            {
                Session["PASSWORD"] = Request.QueryString["Password"].ToString();
            }

            if (Session["PASSWORD"] == null || string.IsNullOrEmpty(Session["PASSWORD"].ToString()) || Session["PASSWORD"].ToString().ToUpper() != "SDFCONDENSE")
            {
                lblPassword.Visible = true;
                txtPassword.Visible = true;
                txtPassword.Focus();
                cmbChangeType.Visible = false;
                RadGrid1.Visible = false;
                lblItemsPerPage.Visible = false;
                txtItemsPerPage.Visible = false;
            }
            else
            {
                lblPassword.Visible = false;
                txtPassword.Visible = false;
                btnLogin.Visible = false;
                lblError.Visible = false;
                cmbChangeType.Visible = true;
                RadGrid1.Visible = true;
                lblItemsPerPage.Visible = true;
                txtItemsPerPage.Visible = true;
            }

            RebindGrid();
        }

        private void RebindGrid()
        {
            string strChangeType = cmbChangeType.Text;
            if (string.IsNullOrEmpty(strChangeType))
                strChangeType = "Patient Differences Only";


            SqlDataSource.SelectCommand = "Select top " + txtItemsPerPage.Text + " * from vwpatientinfo where status = '" + strChangeType + "' and processedtext != 'Processed'";

            lblUserName.Text = GetUserName();

            //GridSortExpression descriptor = new GridSortExpression();

            //descriptor.FieldName = "Processed";

            //descriptor.SortOrder = GridSortOrder.Ascending;
            //RadGrid1.MasterTableView.AllowCustomSorting = false;
            //RadGrid1.MasterTableView.SortExpressions.Clear();
            //RadGrid1.MasterTableView.SortExpressions.Add(descriptor);
            RadGrid1.Rebind();
        }

        private string GetUserName()
        {
            string strLoggedInUserID = HttpContext.Current.User.Identity.Name;

            //Debug Only
            if (string.IsNullOrEmpty(strLoggedInUserID))
            {
                strLoggedInUserID = "AVaughan@stdavidsfoundation.org";
                strLoggedInUserID = "jimmy.voegele@gmail.com";
            }

            return strLoggedInUserID;
        }

        protected void RadGrid1_DataBound(object sender, EventArgs e)
        {
            int lCount = 0;
            foreach (Telerik.Web.UI.GridDataItem itm in RadGrid1.MasterTableView.Items)
            {

                if (itm["Status"].Text.Contains("Differences") && !itm["ProcessedText"].Text.Equals("Processed"))
                {
                    RadGrid1.MasterTableView.Items[lCount].BackColor = Color.Red;
                    RadGrid1.MasterTableView.Items[lCount].ForeColor = Color.White;
                }
                else if (itm["Status"].Text == "No Record")
                {
                    RadGrid1.MasterTableView.Items[lCount].BackColor = Color.Yellow;
                }
                else if (itm["ProcessedText"].Text.Equals("Processed"))
                {
                    if (itm["Status"].Text != "No Records" && itm["Status"].Text != "No Action Required")
                    {
                        RadGrid1.MasterTableView.Items[lCount].BackColor = Color.Blue;
                        RadGrid1.MasterTableView.Items[lCount].ForeColor = Color.White;
                    }
                }
                lCount++;
            }
        }

        protected void cmbChangeYear_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RebindGrid();
        }

        protected void txtItemsPerPage_TextChanged(object sender, EventArgs e)
        {
            RebindGrid();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text.ToUpper() == "SDFCONDENSE")
            {
                Session["PASSWORD"] = "SDFCONDENSE";
                lblPassword.Visible = false;
                txtPassword.Visible = false;
                btnLogin.Visible = false;
                lblError.Visible = false;
                cmbChangeType.Visible = true;
                RadGrid1.Visible = true;
                lblItemsPerPage.Visible = true;
                txtItemsPerPage.Visible = true;

                RebindGrid();
            }
            else
            {
                lblError.Text = "You have entered an incorrect password.";
                lblError.Visible = true;
            }
        }
    }
}