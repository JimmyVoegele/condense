﻿<%@ Page Title="Student Differences" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Condense._Default"   EnableEventValidation="false"%>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h2 style="color:#0091ba">Condense - Dental Patient Data Verification Application</h2>
    <br />
    <asp:label runat="server" ID="lblUser1" text="Current User:"></asp:label>
    <asp:label runat="server" ID="lblUserName" text=""></asp:label>
    <br />
    <br />
    <asp:label runat="server" ID="lblPassword" text="Please enter the password to access the check-in page:"></asp:label>
    <asp:textbox runat="server" ID="txtPassword" Width="184px" TextMode="Password"></asp:textbox>
    <telerik:RadButton runat="server" ID="btnLogin" text="Log In" OnClick="btnLogin_Click"/>
    <asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="False"></asp:Label>
    <telerik:RadComboBox ID="cmbSchools" runat="server" OnSelectedIndexChanged="cmbSchools_SelectedIndexChanged" DataSourceID="SqlDataSource1" DataValueField="ID" DataTextField="name" AutoPostBack="true" Width="250px">
        <Items>
            <telerik:RadComboBoxItem Text="All Schools" />
        </Items>
    </telerik:RadComboBox>
    <br />
    <telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource" OnDataBound ="RadGrid1_DataBound">
        <MasterTableView AutoGenerateColumns="false" AllowSorting="true" CommandItemDisplay="TopAndBottom">
            <CommandItemSettings ShowRefreshButton="true" ShowAddNewRecordButton="false" ShowCancelChangesButton="false" ShowSaveChangesButton="false" />
            <Columns>
                <telerik:GridCheckBoxColumn DataField="Processed"  HeaderText="Proc" HeaderStyle-Width="50px"></telerik:GridCheckBoxColumn>
                <telerik:GridBoundColumn DataField="ProcessedText"  HeaderText="ProcessedText" ItemStyle-Width="200px" HeaderStyle-Width="1px"></telerik:GridBoundColumn>
                <telerik:GridHyperLinkColumn HeaderStyle-Width="100px" ItemStyle-ForeColor="Blue" ItemStyle-Font-Underline="true" DataTextFormatString="{0}" Target="_blank" DataNavigateUrlFields="PatNum" DataNavigateUrlFormatString="~/Student.aspx?PatNum={0}" DataTextField="PatNum" HeaderText="Patient Number" SortExpression="PatNum" UniqueName="PatNum">
                </telerik:GridHyperLinkColumn>
                <telerik:GridBoundColumn DataField="Status"  HeaderText="Status"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FNAME"  HeaderText="First Name"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LNAME"  HeaderText="Last Name"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BirthDate"  HeaderText="Date of Birth"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>

    <asp:SqlDataSource ID="SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:CondenseConnection %>"
        ProviderName="System.Data.SqlClient" 
        SelectCommand="Select * from vwDentalStudentsBySchool">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CondenseConnection %>"
        ProviderName="System.Data.SqlClient" 
        SelectCommand="Select  * from vwDentalEOSSchools">
    </asp:SqlDataSource>
</asp:Content>
