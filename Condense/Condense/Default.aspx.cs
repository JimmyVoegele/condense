﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using Telerik.Web.UI;

namespace Condense
{
    public partial class _Default : Page
    {
        //static string m_strStartTime = "";
        //static string m_strEndTime = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Password"]))
            {
                Session["PASSWORD"] = Request.QueryString["Password"].ToString();
            }

            if (Session["PASSWORD"] == null || string.IsNullOrEmpty(Session["PASSWORD"].ToString()) || Session["PASSWORD"].ToString().ToUpper() != "SDFCONDENSE")
            {
                lblPassword.Visible = true;
                txtPassword.Visible = true;
                txtPassword.Focus();
                cmbSchools.Visible = false;
                RadGrid1.Visible = false;
            }
            else
            {
                lblPassword.Visible = false;
                txtPassword.Visible = false;
                btnLogin.Visible = false;
                lblError.Visible = false;
                cmbSchools.Visible = true;
                RadGrid1.Visible = true;

                string strSchool = cmbSchools.Text;

                if (string.IsNullOrEmpty(strSchool))
                    strSchool = "Allison Elem";

                SqlDataSource.SelectCommand = "Select  * from vwDentalStudentsBySchool where name = '" + strSchool + "'";

                GridSortExpression descriptor = new GridSortExpression();

                descriptor.FieldName = "Processed";

                descriptor.SortOrder = GridSortOrder.Ascending;
                RadGrid1.MasterTableView.AllowCustomSorting = false;
                RadGrid1.MasterTableView.SortExpressions.Clear();
                RadGrid1.MasterTableView.SortExpressions.Add(descriptor);

                RadGrid1.Rebind();


            }

            lblUserName.Text = GetUserName();


        }

        private string GetUserName()
        {
            string strLoggedInUserID = HttpContext.Current.User.Identity.Name;

            strLoggedInUserID = Context.User.Identity.Name.ToString();

            //Debug Only
            if (string.IsNullOrEmpty(strLoggedInUserID))
            {
                strLoggedInUserID = "AVaughan@stdavidsfoundation.org";
                strLoggedInUserID = "jimmy.voegele@gmail.com";
            }

            return strLoggedInUserID;
        }



        protected void RadGrid1_DataBound(object sender, EventArgs e)
        {
            int lCount = 0;
            foreach (Telerik.Web.UI.GridDataItem itm in RadGrid1.MasterTableView.Items)
            {
                if (itm["Status"].Text.Contains("Differences") && !itm["ProcessedText"].Text.Equals("Processed"))
                {
                    RadGrid1.MasterTableView.Items[lCount].BackColor = Color.Red;
                    RadGrid1.MasterTableView.Items[lCount].ForeColor = Color.White;
                }
                else if (itm["Status"].Text == "No Record")
                {
                    RadGrid1.MasterTableView.Items[lCount].BackColor = Color.Yellow;
                }else if (itm["ProcessedText"].Text.Equals("Processed") )
                {
                    if (itm["Status"].Text != "No Records" && itm["Status"].Text != "No Action Required")
                    {
                        RadGrid1.MasterTableView.Items[lCount].BackColor = Color.Blue;
                        RadGrid1.MasterTableView.Items[lCount].ForeColor = Color.White;
                    }
                }

                lCount++;
            }
        }

        protected void cmbSchools_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            SqlDataSource.SelectCommand = "Select  * from vwDentalStudentsBySchool where name = '" + cmbSchools.Text + "'";
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text.ToUpper() == "SDFCONDENSE")
            {
                Session["PASSWORD"] = "SDFCONDENSE";
                lblPassword.Visible = false;
                txtPassword.Visible = false;
                btnLogin.Visible = false;
                lblError.Visible = false;
                cmbSchools.Visible = true;
                RadGrid1.Visible = true;
                string strSchool = cmbSchools.Text;

                if (string.IsNullOrEmpty(strSchool))
                    strSchool = "Allison Elem";

                SqlDataSource.SelectCommand = "Select  * from vwDentalStudentsBySchool where name = '" + strSchool + "'";

                GridSortExpression descriptor = new GridSortExpression();

                descriptor.FieldName = "Processed";

                descriptor.SortOrder = GridSortOrder.Ascending;
                RadGrid1.MasterTableView.AllowCustomSorting = false;
                RadGrid1.MasterTableView.SortExpressions.Clear();
                RadGrid1.MasterTableView.SortExpressions.Add(descriptor);

                RadGrid1.Rebind();
            }
            else
            {
                lblError.Text = "You have entered an incorrect password.";
                lblError.Visible = true;
            }
        }
    }
}