﻿
using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;

namespace Condense
{
    public class cDentalStudentLookUp
    {
        private string strSourceDatabase = "BARTON_Opendent";
        private string strT1Database = "T1_Opendent";
        private string strT3Database = "T3_Opendent";
        private string strT4Database = "T4_Opendent";
        private string strT5Database = "T5_Opendent";
        private string strT6Database = "T6_Opendent";
        private string strT7Database = "T7_Opendent";
        private string strT8Database = "T8_Opendent";
        private string strT9Database = "T9_Opendent";
        private string strT10Database = "T10_Opendent";
        private string strT11Database = "T11_Opendent";
        private string strT12Database = "T12_Opendent"; 

        public int IsStudentDifferent(long nPatNum)
        {

            bool bDifferent = false;

            //0 - No Differnces
            //1 -- Patient Differnces
            //Even -- Clinical Differences only
            //Odd and greater than 1 - Patient and Clinical Differnces
            int iDifferent = 0;

            DataTable dtCompare = null;

            //Remove Student Info Check for now...
            dtCompare = GetStudentInfo(nPatNum, true);
            dtCompare.Columns.Remove("SSN");
            dtCompare.Columns.Remove("CreditType");
            dtCompare.Columns.Remove("EstBalance");
            dtCompare.Columns.Remove("BillingType");
            dtCompare.Columns.Remove("MedicaidID");
            dtCompare.Columns.Remove("Bal_0_30");
            dtCompare.Columns.Remove("Bal_31_60");
            dtCompare.Columns.Remove("Bal_61_90");
            dtCompare.Columns.Remove("BalOver90");
            dtCompare.Columns.Remove("InsEst");
            dtCompare.Columns.Remove("BalTotal");
            dtCompare.Columns.Remove("EmployerNum");
            dtCompare.Columns.Remove("EmploymentNote");
            dtCompare.Columns.Remove("HasIns");
            dtCompare.Columns.Remove("TrophyFolder");
            dtCompare.Columns.Remove("PlannedIsDone");
            dtCompare.Columns.Remove("Premed");
            dtCompare.Columns.Remove("Ward");
            dtCompare.Columns.Remove("PreferConfirmMethod");
            dtCompare.Columns.Remove("PreferContactMethod");
            dtCompare.Columns.Remove("PreferRecallMethod");
            dtCompare.Columns.Remove("SchedBeforeTime");
            dtCompare.Columns.Remove("SchedAfterTime");
            dtCompare.Columns.Remove("SchedDayOfWeek");
            dtCompare.Columns.Remove("AdmitDate");
            dtCompare.Columns.Remove("Title");
            dtCompare.Columns.Remove("PayPlanDue");
            dtCompare.Columns.Remove("DateTStamp");
            dtCompare.Columns.Remove("ResponsParty");
            dtCompare.Columns.Remove("CanadianEligibilityCode");
            dtCompare.Columns.Remove("AskToArriveEarly");
            dtCompare.Columns.Remove("OnlinePassword");
            dtCompare.Columns.Remove("PreferContactConfidential");
            dtCompare.Columns.Remove("SuperFamily");
            dtCompare.Columns.Remove("TxtMsgOk");
            dtCompare.Columns.Remove("SmokingSnoMed");
            dtCompare.Columns.Remove("Country");
            dtCompare.Columns.Remove("DateTimeDeceased");
            dtCompare.Columns.Remove("priprov");




            bDifferent = IsDataRowEqual(dtCompare);

            if (!bDifferent) iDifferent = 1;

            dtCompare = GetPatientNotes(nPatNum);
            bDifferent = IsDataRowEqual(dtCompare);
            if (!bDifferent) return iDifferent + 2;
            
            dtCompare = GetMedication(nPatNum);
            bDifferent = IsDataRowEqual(dtCompare);
            if (!bDifferent) return iDifferent + 2;

            dtCompare = GetDisease(nPatNum);
            bDifferent = IsDataRowEqual(dtCompare);
            if (!bDifferent) return iDifferent + 2;

            dtCompare = GetAllergy(nPatNum);
            bDifferent = IsDataRowEqual(dtCompare);
            if (!bDifferent) return iDifferent + 2;

            dtCompare = GetProcedureLog(nPatNum);
            bDifferent = IsDataRowEqual(dtCompare);
            if (!bDifferent) return iDifferent + 2;

            dtCompare = GetProcedureNote(nPatNum);
            bDifferent = IsDataRowEqual(dtCompare);
            if (!bDifferent) return iDifferent + 2;

            return iDifferent;

        }

        public string TruncateandInsertData(string strTableName, DataTable DTResults, long nPatNum)
        {
            try
            {

                bool bErrorTrucating = false;


                string strConnectionString = "Server=aus-db01; Initial Catalog = SDFCondense; User ID = jvoegele; Password = Austin2018!;";


                //Truncate Organizations Table 
                string selectSQL = string.Empty;
                if (nPatNum > 0)
                {
                    selectSQL = "Delete from " + strTableName + " where PatNum = " + nPatNum.ToString();
                    SqlConnection conn = new SqlConnection(strConnectionString);
                    SqlCommand cmd = new SqlCommand(selectSQL, conn);
                    try
                    {
                        conn.Open();
                        //cmd.ExecuteScalar();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Debug");
                        bErrorTrucating = true;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

                if (DTResults != null && DTResults.Rows.Count > 0)
                {
                    if (!bErrorTrucating)
                    {
                        try
                        {
                            SqlBulkCopy sqlbulk = new SqlBulkCopy(strConnectionString);
                            sqlbulk.DestinationTableName = strTableName;
                            sqlbulk.WriteToServer(DTResults);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return "Failed with Error: " + ex.Message;
            }

            return "success";
        }

        public bool IsDataRowEqual(DataTable dtCompare)
        {
            Hashtable htBarton = new Hashtable();
            foreach (DataRow drBarton in dtCompare.Rows)
            {
                if (drBarton[0].ToString() == "BARTON_Opendent")
                {

                    foreach (DataRow dr in dtCompare.Rows)
                    {
                        //if (htIgnoredFields != null)
                        //{
                        //    foreach (DictionaryEntry str in htIgnoredFields)
                        //    {
                        //        string strColName = str.Key.ToString();
                        //        dr[strColName] = drBarton[strColName];
                        //    }
                        //}
                        if (dr[0].ToString() != "BARTON_Opendent")
                        {
                            string strBartonFull = string.Join("|", drBarton.ItemArray.Select(p => p.ToString()).ToArray());
                            string strTDBFull = string.Join("|", dr.ItemArray.Select(p => p.ToString()).ToArray());

                            strBartonFull = strBartonFull.Replace("BARTON_Opendent", "");
                            strTDBFull = strTDBFull.Replace("T1_Opendent", "").Replace("T3_Opendent", "").Replace("T4_Opendent", "").Replace("T5_Opendent", "").Replace("T6_Opendent", "").Replace("T7_Opendent", "").Replace("T8_Opendent", "").Replace("T9_Opendent", "").Replace("T10_Opendent", "").Replace("T11_Opendent", "").Replace("T12_Opendent", "");

                            if (dr[2].ToString() == drBarton[2].ToString() && strBartonFull != strTDBFull)
                            {
                                return false;
                            }
                        }
                    }
                    
                }
            }



            return true;
        }

        public DataTable GetStudentInfo(long nPatNum, bool GetAllDBs)
        {
            

            string strSourceConnectionString = "SERVER=10.1.43.215;DATABASE=" + strSourceDatabase + ";UID=adm_jvoegele;PASSWORD=Austin2018!;SslMode=none;Command Timeout=0";
            DataTable dt =  GetRecords(strSourceConnectionString, "patient", strSourceDatabase, nPatNum, "*", "");
            DataTable dtCombinedStudent = dt;
            if (GetAllDBs)
            {
                string strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT1Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
                DataTable dtT1Student = GetRecords(strTConnectionString, "patient", strT1Database, nPatNum, "*", "");
                dtCombinedStudent.Merge(dtT1Student);

                strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT3Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
                DataTable dtT3Student = GetRecords(strTConnectionString, "patient", strT3Database, nPatNum, "*", "");
                dtCombinedStudent.Merge(dtT3Student);

                strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT4Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
                DataTable dtT4Student = GetRecords(strTConnectionString, "patient", strT4Database, nPatNum, "*", "");
                dtCombinedStudent.Merge(dtT4Student);

                strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT5Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
                DataTable dtT5Student = GetRecords(strTConnectionString, "patient", strT5Database, nPatNum, "*", "");
                dtCombinedStudent.Merge(dtT5Student);

                strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT6Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
                DataTable dtT6Student = GetRecords(strTConnectionString, "patient", strT6Database, nPatNum, "*", "");
                dtCombinedStudent.Merge(dtT6Student);

                strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT7Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
                DataTable dtT7Student = GetRecords(strTConnectionString, "patient", strT7Database, nPatNum, "*", "");
                dtCombinedStudent.Merge(dtT7Student);

                strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT8Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
                DataTable dtT8Student = GetRecords(strTConnectionString, "patient", strT8Database, nPatNum, "*", "");
                dtCombinedStudent.Merge(dtT8Student);

                strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT9Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
                DataTable dtT9Student = GetRecords(strTConnectionString, "patient", strT9Database, nPatNum, "*", "");
                dtCombinedStudent.Merge(dtT9Student);

                strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT10Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
                DataTable dtT10Student = GetRecords(strTConnectionString, "patient", strT10Database, nPatNum, "*", "");
                dtCombinedStudent.Merge(dtT10Student);

                strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT11Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
                DataTable dtT11Student = GetRecords(strTConnectionString, "patient", strT11Database, nPatNum, "*", "");
                dtCombinedStudent.Merge(dtT11Student);

                strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT12Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
                DataTable dtT12Student = GetRecords(strTConnectionString, "patient", strT12Database, nPatNum, "*", "");
                dtCombinedStudent.Merge(dtT12Student);
            }
            return dtCombinedStudent;
        }

        public DataTable GetStudentInfoSQL(long nPatNum, bool GetAllDBs)
        {


            string strSourceConnectionString = ConfigurationManager.ConnectionStrings["CondenseConnection"].ToString();
            DataTable dt = null;
            if (GetAllDBs)
                dt = GetRecordsSQL(strSourceConnectionString, "patient", strT1Database, nPatNum, "*", "");
            else
                dt = GetRecordsSQL(strSourceConnectionString, "patient", strSourceDatabase, nPatNum, "*", "");


            return dt;
        }

        public DataTable GetPatientNotes(long PatNum)
        {
            //Notes
            string strSourceConnectionString = "SERVER=10.1.43.215;DATABASE=" + strSourceDatabase + ";UID=adm_jvoegele;PASSWORD=Austin2018!;SslMode=none;Command Timeout=0";
            DataTable dtBartonNotes = GetRecords(strSourceConnectionString, "patientnote", strSourceDatabase, PatNum, "*", "");

            DataTable dtCombinedNotes = dtBartonNotes;

            string strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT1Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT1Notes = GetRecords(strTConnectionString, "patientnote", strT1Database, PatNum, "*", "");
            dtCombinedNotes.Merge(dtT1Notes);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT3Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT3Notes = GetRecords(strTConnectionString, "patientnote", strT3Database, PatNum, "*", "");
            dtCombinedNotes.Merge(dtT3Notes);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT4Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT4Notes = GetRecords(strTConnectionString, "patientnote", strT4Database, PatNum, "*", "");
            dtCombinedNotes.Merge(dtT4Notes);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT5Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT5Notes = GetRecords(strTConnectionString, "patientnote", strT5Database, PatNum, "*", "");
            dtCombinedNotes.Merge(dtT5Notes);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT6Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT6Notes = GetRecords(strTConnectionString, "patientnote", strT6Database, PatNum, "*", "");
            dtCombinedNotes.Merge(dtT6Notes);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT7Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT7Notes = GetRecords(strTConnectionString, "patientnote", strT7Database, PatNum, "*", "");
            dtCombinedNotes.Merge(dtT7Notes);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT8Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT8Notes = GetRecords(strTConnectionString, "patientnote", strT8Database, PatNum, "*", "");
            dtCombinedNotes.Merge(dtT8Notes);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT9Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT9Notes = GetRecords(strTConnectionString, "patientnote", strT9Database, PatNum, "*", "");
            dtCombinedNotes.Merge(dtT9Notes);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT10Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT10Notes = GetRecords(strTConnectionString, "patientnote", strT10Database, PatNum, "*", "");
            dtCombinedNotes.Merge(dtT10Notes);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT11Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT11Notes = GetRecords(strTConnectionString, "patientnote", strT11Database, PatNum, "*", "");
            dtCombinedNotes.Merge(dtT11Notes);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT12Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT12Notes = GetRecords(strTConnectionString, "patientnote", strT12Database, PatNum, "*", "");
            dtCombinedNotes.Merge(dtT12Notes);

            return dtCombinedNotes;
        }

        public DataTable GetPatientNotesSQL(long PatNum)
        {
            //Notes
            string strSourceConnectionString = ConfigurationManager.ConnectionStrings["CondenseConnection"].ToString();
            DataTable dt = null;
            dt = GetRecordsSQL(strSourceConnectionString, "patientnote", strT1Database, PatNum, "*", "");


            return dt;
        }

        public DataTable GetMedication(long nPatNum)
        {
            string strSelectStatement = "MedicationPatNum,PatNum,medication.MedicationNum,PatNote,medication.DateTStamp, DateStart,DateStop,ProvNum,MedDescript,NewCropGuid,IsCpoe,MedName,GenericNum,Notes,medication.RxCui";
            string strJoinStatement = " left join medication on medicationpat.medicationnum = medication.medicationnum ";
            //Medication

            string strSourceConnectionString = "SERVER=10.1.43.215;DATABASE=" + strSourceDatabase + ";UID=adm_jvoegele;PASSWORD=Austin2018!;SslMode=none;Command Timeout=0";

            DataTable dtBartonMedication = GetRecords(strSourceConnectionString, "medicationpat", strSourceDatabase, nPatNum, strSelectStatement, strJoinStatement);

            DataTable dtCombinedMedication = dtBartonMedication;

            string strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT1Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT1Medication = GetRecords(strTConnectionString, "medicationpat", strT1Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedMedication.Merge(dtT1Medication);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT3Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT3Medication = GetRecords(strTConnectionString, "medicationpat", strT3Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedMedication.Merge(dtT3Medication);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT4Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT4Medication = GetRecords(strTConnectionString, "medicationpat", strT4Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedMedication.Merge(dtT4Medication);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT5Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT5Medication = GetRecords(strTConnectionString, "medicationpat", strT5Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedMedication.Merge(dtT5Medication);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT6Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT6Medication = GetRecords(strTConnectionString, "medicationpat", strT6Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedMedication.Merge(dtT6Medication);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT7Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT7Medication = GetRecords(strTConnectionString, "medicationpat", strT7Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedMedication.Merge(dtT7Medication);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT8Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT8Medication = GetRecords(strTConnectionString, "medicationpat", strT8Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedMedication.Merge(dtT8Medication);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT9Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT9Medication = GetRecords(strTConnectionString, "medicationpat", strT9Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedMedication.Merge(dtT9Medication);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT10Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT10Medication = GetRecords(strTConnectionString, "medicationpat", strT10Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedMedication.Merge(dtT10Medication);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT11Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT11Medication = GetRecords(strTConnectionString, "medicationpat", strT11Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedMedication.Merge(dtT11Medication);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT12Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT12Medication = GetRecords(strTConnectionString, "medicationpat", strT12Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedMedication.Merge(dtT12Medication);

            return dtCombinedMedication;
        }

        public DataTable GetMedicationSQL(long nPatNum)
        {


            string strSourceConnectionString = ConfigurationManager.ConnectionStrings["CondenseConnection"].ToString();
            DataTable dt = null;
            dt = GetRecordsSQL(strSourceConnectionString, "medicationpat", strT1Database, nPatNum, "*", "");


            return dt;
        }

        public DataTable GetDisease(long nPatNum)
        {
            string strSelectStatement = "DiseaseNum,PatNum,disease.DiseaseDefNum,PatNote,disease.DateTStamp,ProbStatus,DateStart,DateStop,SnomedProblemType,FunctionStatus,DiseaseName,ItemOrder,IsHidden,ICD9Code,SnomedCode,Icd10Code ";
            string strJoinStatement = "  left join diseasedef on disease.DiseaseDefNum = diseasedef.DiseaseDefNum ";

            //Disease

            string strSourceConnectionString = "SERVER=10.1.43.215;DATABASE=" + strSourceDatabase + ";UID=adm_jvoegele;PASSWORD=Austin2018!;SslMode=none;Command Timeout=0";

            DataTable dtBartonDisease = GetRecords(strSourceConnectionString, "disease", strSourceDatabase, nPatNum, strSelectStatement, strJoinStatement);

            DataTable dtCombinedDisease = dtBartonDisease;

            string strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT1Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT1Disease = GetRecords(strTConnectionString, "disease", strT1Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedDisease.Merge(dtT1Disease);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT3Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT3Disease = GetRecords(strTConnectionString, "disease", strT3Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedDisease.Merge(dtT3Disease);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT4Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT4Disease = GetRecords(strTConnectionString, "disease", strT4Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedDisease.Merge(dtT4Disease);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT5Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT5Disease = GetRecords(strTConnectionString, "disease", strT5Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedDisease.Merge(dtT5Disease);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT6Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT6Disease = GetRecords(strTConnectionString, "disease", strT6Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedDisease.Merge(dtT6Disease);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT7Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT7Disease = GetRecords(strTConnectionString, "disease", strT7Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedDisease.Merge(dtT7Disease);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT8Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT8Disease = GetRecords(strTConnectionString, "disease", strT8Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedDisease.Merge(dtT8Disease);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT9Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT9Disease = GetRecords(strTConnectionString, "disease", strT9Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedDisease.Merge(dtT9Disease);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT10Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT10Disease = GetRecords(strTConnectionString, "disease", strT10Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedDisease.Merge(dtT10Disease);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT11Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT11Disease = GetRecords(strTConnectionString, "disease", strT11Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedDisease.Merge(dtT11Disease);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT12Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT12Disease = GetRecords(strTConnectionString, "disease", strT12Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedDisease.Merge(dtT12Disease);

            return dtCombinedDisease;
        }

        public DataTable GetDiseaseSQL(long nPatNum)
        {

            string strSourceConnectionString = ConfigurationManager.ConnectionStrings["CondenseConnection"].ToString();
            DataTable dt = null;
            dt = GetRecordsSQL(strSourceConnectionString, "disease", strT1Database, nPatNum, "*", "");


            return dt;
        }

        public DataTable GetAllergy(long nPatNum)
        {
            string strSelectStatement = "AllergyNum,allergy.AllergyDefNum,PatNum,Reaction,StatusIsActive,allergy.DateTStamp,DateAdverseReaction,SnomedReaction,Description,IsHidden,SnomedType,MedicationNum,UniiCode";
            string strJoinStatement = " left join allergydef on allergy.AllergyDefNum = allergydef.AllergyDefNum ";

            //Allergy

            string strSourceConnectionString = "SERVER=10.1.43.215;DATABASE=" + strSourceDatabase + ";UID=adm_jvoegele;PASSWORD=Austin2018!;SslMode=none;Command Timeout=0";

            DataTable dtBartonAllergy = GetRecords(strSourceConnectionString, "allergy", strSourceDatabase, nPatNum, strSelectStatement, strJoinStatement);

            DataTable dtCombinedAllergy = dtBartonAllergy;

            string strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT1Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT1Allergy = GetRecords(strTConnectionString, "allergy", strT1Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedAllergy.Merge(dtT1Allergy);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT3Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT3Allergy = GetRecords(strTConnectionString, "allergy", strT3Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedAllergy.Merge(dtT3Allergy);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT4Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT4Allergy = GetRecords(strTConnectionString, "allergy", strT4Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedAllergy.Merge(dtT4Allergy);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT5Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT5Allergy = GetRecords(strTConnectionString, "allergy", strT5Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedAllergy.Merge(dtT5Allergy);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT6Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT6Allergy = GetRecords(strTConnectionString, "allergy", strT6Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedAllergy.Merge(dtT6Allergy);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT7Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT7Allergy = GetRecords(strTConnectionString, "allergy", strT7Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedAllergy.Merge(dtT7Allergy);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT8Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT8Allergy = GetRecords(strTConnectionString, "allergy", strT8Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedAllergy.Merge(dtT8Allergy);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT9Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT9Allergy = GetRecords(strTConnectionString, "allergy", strT9Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedAllergy.Merge(dtT9Allergy);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT10Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT10Allergy = GetRecords(strTConnectionString, "allergy", strT10Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedAllergy.Merge(dtT10Allergy);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT11Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT11Allergy = GetRecords(strTConnectionString, "allergy", strT11Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedAllergy.Merge(dtT11Allergy);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT12Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT12Allergy = GetRecords(strTConnectionString, "allergy", strT12Database, nPatNum, strSelectStatement, strJoinStatement);
            dtCombinedAllergy.Merge(dtT12Allergy);

            return dtCombinedAllergy;
        }

        public DataTable GetAllergySQL(long nPatNum)
        {

            string strSourceConnectionString = ConfigurationManager.ConnectionStrings["CondenseConnection"].ToString();
            DataTable dt = null;
            dt = GetRecordsSQL(strSourceConnectionString, "allergy", strT1Database, nPatNum, "*", "");


            return dt;
        }

        public DataTable GetProcedureLog(long nPatNum)
        {
            //ProcedureLog

            string strSourceConnectionString = "SERVER=10.1.43.215;DATABASE=" + strSourceDatabase + ";UID=adm_jvoegele;PASSWORD=Austin2018!;SslMode=none;Command Timeout=0";

            DataTable dtBartonProcedureLog = GetRecords(strSourceConnectionString, "procedurelog", strSourceDatabase, nPatNum, "*", "");

            DataTable dtCombinedProcedureLog = dtBartonProcedureLog;

            string strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT1Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT1ProcedureLog = GetRecords(strTConnectionString, "procedurelog", strT1Database, nPatNum, "*", "");
            dtCombinedProcedureLog.Merge(dtT1ProcedureLog);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT3Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT3ProcedureLog = GetRecords(strTConnectionString, "procedurelog", strT3Database, nPatNum, "*", "");
            dtCombinedProcedureLog.Merge(dtT3ProcedureLog);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT4Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT4ProcedureLog = GetRecords(strTConnectionString, "procedurelog", strT4Database, nPatNum, "*", "");
            dtCombinedProcedureLog.Merge(dtT4ProcedureLog);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT5Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT5ProcedureLog = GetRecords(strTConnectionString, "procedurelog", strT5Database, nPatNum, "*", "");
            dtCombinedProcedureLog.Merge(dtT5ProcedureLog);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT6Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT6ProcedureLog = GetRecords(strTConnectionString, "procedurelog", strT6Database, nPatNum, "*", "");
            dtCombinedProcedureLog.Merge(dtT6ProcedureLog);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT7Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT7ProcedureLog = GetRecords(strTConnectionString, "procedurelog", strT7Database, nPatNum, "*", "");
            dtCombinedProcedureLog.Merge(dtT7ProcedureLog);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT8Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT8ProcedureLog = GetRecords(strTConnectionString, "procedurelog", strT8Database, nPatNum, "*", "");
            dtCombinedProcedureLog.Merge(dtT8ProcedureLog);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT9Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT9ProcedureLog = GetRecords(strTConnectionString, "procedurelog", strT9Database, nPatNum, "*", "");
            dtCombinedProcedureLog.Merge(dtT9ProcedureLog);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT10Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT10ProcedureLog = GetRecords(strTConnectionString, "procedurelog", strT10Database, nPatNum, "*", "");
            dtCombinedProcedureLog.Merge(dtT10ProcedureLog);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT11Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT11ProcedureLog = GetRecords(strTConnectionString, "procedurelog", strT11Database, nPatNum, "*", "");
            dtCombinedProcedureLog.Merge(dtT11ProcedureLog);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT12Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT12ProcedureLog = GetRecords(strTConnectionString, "procedurelog", strT12Database, nPatNum, "*", "");
            dtCombinedProcedureLog.Merge(dtT12ProcedureLog);

            return dtCombinedProcedureLog;
        }

        public DataTable GetProcedureLogSQL(long nPatNum)
        {
            //ProcedureLog
            string strSourceConnectionString = ConfigurationManager.ConnectionStrings["CondenseConnection"].ToString();
            DataTable dt = null;
            dt = GetRecordsSQL(strSourceConnectionString, "procedurelog", strT1Database, nPatNum, "*", "");


            return dt;
        }

        public DataTable GetProcedureNote(long nPatNum)
        {
            //ProcedureNote

            string strSourceConnectionString = "SERVER=10.1.43.215;DATABASE=" + strSourceDatabase + ";UID=adm_jvoegele;PASSWORD=Austin2018!;SslMode=none;Command Timeout=0";

            DataTable dtBartonProcedureNote = GetRecords(strSourceConnectionString, "procnote", strSourceDatabase, nPatNum, "*", "");

            DataTable dtCombinedProcedureNote = dtBartonProcedureNote;

            string strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT1Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT1ProcedureNote = GetRecords(strTConnectionString, "procnote", strT1Database, nPatNum, "*", "");
            dtCombinedProcedureNote.Merge(dtT1ProcedureNote);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT3Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT3ProcedureNote = GetRecords(strTConnectionString, "procnote", strT3Database, nPatNum, "*", "");
            dtCombinedProcedureNote.Merge(dtT3ProcedureNote);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT4Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT4ProcedureNote = GetRecords(strTConnectionString, "procnote", strT4Database, nPatNum, "*", "");
            dtCombinedProcedureNote.Merge(dtT4ProcedureNote);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT5Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT5ProcedureNote = GetRecords(strTConnectionString, "procnote", strT5Database, nPatNum, "*", "");
            dtCombinedProcedureNote.Merge(dtT5ProcedureNote);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT6Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT6ProcedureNote = GetRecords(strTConnectionString, "procnote", strT6Database, nPatNum, "*", "");
            dtCombinedProcedureNote.Merge(dtT6ProcedureNote);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT7Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT7ProcedureNote = GetRecords(strTConnectionString, "procnote", strT7Database, nPatNum, "*", "");
            dtCombinedProcedureNote.Merge(dtT7ProcedureNote);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT8Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT8ProcedureNote = GetRecords(strTConnectionString, "procnote", strT8Database, nPatNum, "*", "");
            dtCombinedProcedureNote.Merge(dtT8ProcedureNote);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT9Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT9ProcedureNote = GetRecords(strTConnectionString, "procnote", strT9Database, nPatNum, "*", "");
            dtCombinedProcedureNote.Merge(dtT9ProcedureNote);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT10Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT10ProcedureNote = GetRecords(strTConnectionString, "procnote", strT10Database, nPatNum, "*", "");
            dtCombinedProcedureNote.Merge(dtT10ProcedureNote);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT11Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT11ProcedureNote = GetRecords(strTConnectionString, "procnote", strT11Database, nPatNum, "*", "");
            dtCombinedProcedureNote.Merge(dtT11ProcedureNote);

            strTConnectionString = "SERVER = 10.1.43.215; DATABASE = " + strT12Database + "; UID = adm_jvoegele; PASSWORD = Austin2018!; SslMode = none; Command Timeout = 0";
            DataTable dtT12ProcedureNote = GetRecords(strTConnectionString, "procnote", strT12Database, nPatNum, "*", "");
            dtCombinedProcedureNote.Merge(dtT12ProcedureNote);

            return dtCombinedProcedureNote;
        }

        public DataTable GetProcedureNoteSQL(long nPatNum)
        {
            //ProcedureNote
            string strSourceConnectionString = ConfigurationManager.ConnectionStrings["CondenseConnection"].ToString();
            DataTable dt = null;
            dt = GetRecordsSQL(strSourceConnectionString, "procnote", strT1Database, nPatNum, "*", "");


            return dt;
        }


        public long GetPatientNumber(string strFirstname, string strLastName, string strDOB)
        {
            string strSourceDatabase = "BARTON_Opendent";
            string strSourceConnectionString = "SERVER=10.1.43.215;DATABASE=" + strSourceDatabase + ";UID=adm_jvoegele;PASSWORD=Austin2018!;SslMode=none;Command Timeout=0";

            MySqlConnection connection = new MySqlConnection(strSourceConnectionString);
            connection.Open();
            MySqlCommand mySqlCommand = null;
            string strsql = "";

            

            strsql = "SELECT PatNum FROM patient where FNAME = '" + strFirstname.Replace("'","''") + "' and LNAME = '" + strLastName.Replace("'", "''") + "' and birthdate = '" + strDOB + "' " + " LIMIT 0, 100";


            mySqlCommand = new MySqlCommand(strsql, connection);

            MySqlDataReader reader = mySqlCommand.ExecuteReader();

            string strPatNum = "";

            while (reader.Read())
            {
                strPatNum = reader.GetValue(0).ToString();
            }

            connection.Close();
            reader.Close();
            if (string.IsNullOrEmpty(strPatNum))
                strPatNum = "0";
            return Convert.ToInt64(strPatNum);
        }

        private DataTable GetRecords(string stConnectionString, string strTableName, string strDatabaseName, long PatNum, string strSelectStatement, string strJoinStatement)
        {

            MySqlConnection connection = new MySqlConnection(stConnectionString);
            connection.Open();
            MySqlCommand mySqlCommand = null;
            string strsql = "";
            if (PatNum == 0)
                return null;
            else
                strsql = "SELECT "+ strSelectStatement  + " FROM " + strTableName + strJoinStatement + " where PatNum =" + PatNum + " LIMIT 0, 100";

            mySqlCommand = new MySqlCommand(strsql, connection);

            MySqlDataReader reader = mySqlCommand.ExecuteReader();

            DataTable DTTable = new DataTable(strTableName);

            DataColumn dt = new DataColumn("DataBaseName");
            DTTable.Columns.Add(dt);

            dt = new DataColumn("Status");

            DTTable.Columns.Add(dt);
            for (int i = 0; i < reader.FieldCount; i++)
            {
                string strType = reader.GetFieldType(i).ToString();
                if (!strType.Contains("TimeSpan"))
                {
                    try
                    {
                        DTTable.Columns.Add(reader.GetName(i), reader.GetFieldType(i));
                    }
                    catch { }
                }
                else
                {
                    DTTable.Columns.Add(reader.GetName(i), Type.GetType("System.String"));
                }

            }

            int nCount = 0;

            while (reader.Read())
            {
                DataRow dr = DTTable.NewRow();

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    try
                    {
                        dr[DTTable.Columns[i+2]] = reader.GetValue(i);
                    }
                    catch (Exception ex)
                    {
                        Console.Write("MySQL Error- " + ex.Message);
                    }
                }
                dr["DataBaseName"] = strDatabaseName;
                dr["Status"] = "No Action Required";
                nCount++;

                DTTable.Rows.Add(dr);
            }

            int nCountSlave = DTTable.Rows.Count;

            connection.Close();

            reader.Close();

            return DTTable;
        }

        private DataTable GetRecordsSQL(string stConnectionString, string strTableName, string strDatabaseName, long PatNum, string strSelectStatement, string strJoinStatement)
        {
            string strsql = "";
            if (PatNum == 0)
                return null;
            else
                strsql = "SELECT top 100 " + strSelectStatement + " FROM " + strTableName + strJoinStatement + " where PatNum =" + PatNum;

            if (strDatabaseName == "BARTON_Opendent")
                strsql += " and databasename = 'BARTON_Opendent'";


            SqlConnection conn = new SqlConnection(stConnectionString);
            SqlCommand cmd = new SqlCommand(strsql, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            DataTable DTTable = new DataTable();
            DTTable.Load(reader);

            conn.Close();

            reader.Close();

            return DTTable;
        }

        public DataTable GetDentalStudentsFromDMA(bool bBatchMode)
        {
            string strConnString = ConfigurationManager.AppSettings["DefaultConnection"];

            string strSelectSQL = "";

            if (bBatchMode)
                strSelectSQL = "Select * from [dbo].[vwDentalStudentsBatch]";
            else
                strSelectSQL = "Select * from [dbo].[vwDentalStudents]";

            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand(strSelectSQL, conn);
            SqlDataReader reader;

            DataTable dtStudents = new DataTable("Students");

            dtStudents.Columns.Add("Status");
            dtStudents.Columns.Add("PatNum");
            dtStudents.Columns.Add("FName");
            dtStudents.Columns.Add("LName");
            dtStudents.Columns.Add("BirthDate");
            int lcount = 0;
            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string strFirstName = reader["FNAME"].ToString();
                    string strLastName = reader["LNAME"].ToString();
                    string strDOB = reader["DOB"].ToString();
                    long lPAtNum = GetPatientNumber(strFirstName, strLastName, strDOB);
                    string strStatus = "";
                    string strPAtNum = "";
                    if (lPAtNum == 0)
                    {
                        strPAtNum = "0";
                        strStatus = "No Record";
                    }
                    else
                    {
                        strPAtNum = lPAtNum.ToString();
                        int iStudentDifferent = IsStudentDifferent(lPAtNum);
                        if (iStudentDifferent == 0)
                            strStatus = "No Action Required";
                        else if (iStudentDifferent == 1)
                            strStatus = "Patient Differences Only";
                        else if (iStudentDifferent == 2)
                            strStatus = "Clinical Differences Only";
                        else
                            strStatus = "Patient and Clinical Differences";
                    }
                    dtStudents.Rows.Add(strStatus, strPAtNum, strFirstName, strLastName, strDOB);
                    lcount++;
                }
            }
            catch (Exception ex)
            {

            }

            return dtStudents;
        }
    }
}