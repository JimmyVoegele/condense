﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using Telerik.Web.Data;
using Telerik.Web.UI;

namespace Condense
{
    public partial class About : Page
    {
        static long m_PatID = 0;
        static string m_strConnectionString = "Server=aus-db01; Initial Catalog = SDFCondense; User ID = jvoegele; Password = Austin2018!;";

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                string strPatientNum = Request.QueryString["PatNum"];

                btnProcess.Enabled = !HasStudentBeenProcessed();

                //if (string.IsNullOrEmpty(strPatientNum) )
                //{
                //    lblFirstName.Visible = true;
                //    txtStudentFirstName.Visible = true;
                //    lblLastName.Visible = true;
                //    txtStudentLastName.Visible = true;
                //    lblDOB.Visible = true;
                //    txtStudentDOB.Visible = true;
                //    btnSearch.Visible = true;
                //}else 

                if (strPatientNum == "No Record" || strPatientNum == "0" || strPatientNum == null)
                {
                    RadGridMain.Visible = false;
                    RadTabStrip1.Visible = false;
                    lblStudentNotFound.Visible = true;
                    lblNotes.Visible = false;
                    txtNotes.Visible = false;
                    chkShowDiffernces.Visible = false;
                    btnProcess.Visible = false;
                }
                else
                {
                    ////lblFirstName.Visible = false;
                    ////txtStudentFirstName.Visible = false;
                    ////lblLastName.Visible = false;
                    ////txtStudentLastName.Visible = false;
                    ////lblDOB.Visible = false;
                    ////txtStudentDOB.Visible = false;
                    ////btnSearch.Visible = false;
                    m_PatID = Convert.ToInt64(strPatientNum);
                    LoadGrids();
                }
                RadPageView7.Selected = true;
            }
        }

        private bool HasStudentBeenProcessed()
        {
            bool bProcessed = false;

            string strProcessedText = string.Empty;

            long nPatNum = Convert.ToInt64(Request.QueryString["PatNum"]);
            if (nPatNum > 0)
            {

                string selectSQL = "SELECT ProcessedText,Notes from vwpatientinfo where PatNum =  " + nPatNum;

                SqlConnection conn = new SqlConnection(m_strConnectionString);
                SqlCommand cmd = new SqlCommand(selectSQL, conn);
                SqlDataReader reader;

                try
                {
                    conn.Open();
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        strProcessedText = reader["ProcessedText"].ToString();
                        if (strProcessedText.ToUpper() == "PROCESSED")
                        {
                            bProcessed = true;
                            txtNotes.Text = reader["Notes"].ToString();
                            txtNotes.ReadOnly = true;
                            btnProcess.Text = "Student has been processed";
                        }
                    }
                    reader.Close();
                }
                catch (Exception err)
                {
                    Console.Write(err);
                }
                finally
                {
                    conn.Close();
                }
            }
            return bProcessed;
        }

        protected void HighlighGridDiffernces(RadGrid grd, string strDBKey )
        {
            Console.Write("Found one");

            bool bIdentical = true;

            foreach (GridDataItem dr in grd.MasterTableView.Items)
            {
                if (dr["DatabaseName"].Text.Contains("BARTON"))
                {
                    foreach (GridDataItem drSubRow in grd.MasterTableView.Items)
                    {
                        if (!drSubRow["DatabaseName"].Text.Contains("BARTON"))
                        {
                            if (dr[strDBKey].Text.ToString() == drSubRow[strDBKey].Text.ToString())
                            {
                                bIdentical = true;
                                for (int i = 0; i < dr.Cells.Count ;i++)
                                {
                                    string strTextBarton = dr.Cells[i].Text;
                                    string strTextSub = drSubRow.Cells[i].Text;

                                    strTextBarton = strTextBarton.Replace("BARTON_Opendent", "");
                                    strTextSub = strTextSub.Replace("T1_Opendent", "").Replace("T3_Opendent", "").Replace("T4_Opendent", "").Replace("T5_Opendent", "").Replace("T6_Opendent", "").Replace("T7_Opendent", "").Replace("T8_Opendent", "").Replace("T9_Opendent", "").Replace("T10_Opendent", "").Replace("T11_Opendent", "").Replace("T12_Opendent", "");

                                    if (strTextBarton.Contains("10122844"))
                                        Console.Write("1234");

                                    if (strTextBarton.Replace("\r","").Replace("\n", "") != strTextSub.Replace("\r", "").Replace("\n", ""))
                                    {
                                        drSubRow["Status"].Text = "Differences";
                                        drSubRow.Cells[3].BackColor = Color.Red;
                                        drSubRow.Cells[3].ForeColor = Color.White;
                                        drSubRow.Cells[i].BackColor = Color.Red;
                                        drSubRow.Cells[i].ForeColor = Color.White;
                                        bIdentical = false;
                                    }
                                }
                                if (bIdentical && chkShowDiffernces.Checked)
                                {
                                    drSubRow.Visible = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
 
            //cDentalStudentLookUp DSl = new cDentalStudentLookUp();

            //m_PatID = DSl.GetPatientNumber(txtStudentFirstName.Text, txtStudentLastName.Text, txtStudentDOB.Text);
            //LoadGrids();
        }

        private void LoadGrids()
        {
            cDentalStudentLookUp DSl = new cDentalStudentLookUp();
            //Hashtable htIgnoredFields = null;

            DataTable dtBarton = DSl.GetStudentInfo(m_PatID,false);
            dtBarton.Columns.Remove("SSN");
            dtBarton.Columns.Remove("CreditType");
            dtBarton.Columns.Remove("EstBalance");
            dtBarton.Columns.Remove("BillingType");
            dtBarton.Columns.Remove("MedicaidID");
            dtBarton.Columns.Remove("Bal_0_30");
            dtBarton.Columns.Remove("Bal_31_60");
            dtBarton.Columns.Remove("Bal_61_90");
            dtBarton.Columns.Remove("BalOver90");
            dtBarton.Columns.Remove("InsEst");
            dtBarton.Columns.Remove("BalTotal");
            dtBarton.Columns.Remove("EmployerNum");
            dtBarton.Columns.Remove("EmploymentNote");
            dtBarton.Columns.Remove("HasIns");
            dtBarton.Columns.Remove("TrophyFolder");
            dtBarton.Columns.Remove("PlannedIsDone");
            dtBarton.Columns.Remove("Premed");
            dtBarton.Columns.Remove("Ward");
            dtBarton.Columns.Remove("PreferConfirmMethod");
            dtBarton.Columns.Remove("PreferContactMethod");
            dtBarton.Columns.Remove("PreferRecallMethod");
            dtBarton.Columns.Remove("SchedBeforeTime");
            dtBarton.Columns.Remove("SchedAfterTime");
            dtBarton.Columns.Remove("SchedDayOfWeek");
            dtBarton.Columns.Remove("AdmitDate");
            dtBarton.Columns.Remove("Title");
            dtBarton.Columns.Remove("PayPlanDue");
            dtBarton.Columns.Remove("DateTStamp");
            dtBarton.Columns.Remove("ResponsParty");
            dtBarton.Columns.Remove("CanadianEligibilityCode");
            dtBarton.Columns.Remove("AskToArriveEarly");
            dtBarton.Columns.Remove("OnlinePassword");
            dtBarton.Columns.Remove("PreferContactConfidential");
            dtBarton.Columns.Remove("SuperFamily");
            dtBarton.Columns.Remove("TxtMsgOk");
            dtBarton.Columns.Remove("SmokingSnoMed");
            dtBarton.Columns.Remove("Country");
            dtBarton.Columns.Remove("DateTimeDeceased");
            dtBarton.Columns.Remove("priprov");
            if (dtBarton != null)
            {
                
                RadGridMain.DataSource = dtBarton;
                RadGridMain.Rebind();

                DataTable dtPatient = DSl.GetStudentInfo(m_PatID, true);
                //htIgnoredFields = new Hashtable();

                dtPatient.Columns.Remove("SSN");
                dtPatient.Columns.Remove("CreditType");
                dtPatient.Columns.Remove("EstBalance");
                dtPatient.Columns.Remove("BillingType");
                dtPatient.Columns.Remove("MedicaidID");
                dtPatient.Columns.Remove("Bal_0_30");
                dtPatient.Columns.Remove("Bal_31_60");
                dtPatient.Columns.Remove("Bal_61_90");
                dtPatient.Columns.Remove("BalOver90");
                dtPatient.Columns.Remove("InsEst");
                dtPatient.Columns.Remove("BalTotal");
                dtPatient.Columns.Remove("EmployerNum");
                dtPatient.Columns.Remove("EmploymentNote");
                dtPatient.Columns.Remove("HasIns");
                dtPatient.Columns.Remove("TrophyFolder");
                dtPatient.Columns.Remove("PlannedIsDone");
                dtPatient.Columns.Remove("Premed");
                dtPatient.Columns.Remove("Ward");
                dtPatient.Columns.Remove("PreferConfirmMethod");
                dtPatient.Columns.Remove("PreferContactMethod");
                dtPatient.Columns.Remove("PreferRecallMethod");
                dtPatient.Columns.Remove("SchedBeforeTime");
                dtPatient.Columns.Remove("SchedAfterTime");
                dtPatient.Columns.Remove("SchedDayOfWeek");
                dtPatient.Columns.Remove("AdmitDate");
                dtPatient.Columns.Remove("Title");
                dtPatient.Columns.Remove("PayPlanDue");
                dtPatient.Columns.Remove("DateTStamp");
                dtPatient.Columns.Remove("ResponsParty");
                dtPatient.Columns.Remove("CanadianEligibilityCode");
                dtPatient.Columns.Remove("AskToArriveEarly");
                dtPatient.Columns.Remove("OnlinePassword");
                dtPatient.Columns.Remove("PreferContactConfidential");
                dtPatient.Columns.Remove("SuperFamily");
                dtPatient.Columns.Remove("TxtMsgOk");
                dtPatient.Columns.Remove("SmokingSnoMed");
                dtPatient.Columns.Remove("Country");
                dtPatient.Columns.Remove("DateTimeDeceased");
                dtPatient.Columns.Remove("priprov");


                if (!DSl.IsDataRowEqual(dtPatient))
                {
                    RadTabStrip1.Tabs[0].ForeColor = Color.Red;
                }
                HighlighGridDiffernces(RadGridPatient, "PatNum");
                RadGridPatient.DataSource = dtPatient;

                GridSortExpression descriptor = new GridSortExpression();
                descriptor.FieldName = "PatNum";

                descriptor.SortOrder = GridSortOrder.Ascending;
                RadGridPatient.MasterTableView.AllowCustomSorting = false;
                RadGridPatient.MasterTableView.SortExpressions.Clear();
                RadGridPatient.MasterTableView.SortExpressions.Add(descriptor);

                RadGridPatient.Rebind();
                HighlighGridDiffernces(RadGridPatient, "PatNum");

                DataTable dtCombinedNotes = DSl.GetPatientNotes(m_PatID);
                if (!DSl.IsDataRowEqual(dtCombinedNotes))
                {
                    RadTabStrip1.Tabs[1].ForeColor = Color.Red;
                }
                HighlighGridDiffernces(RadGridPatientNotes, "PatNum");
                RadGridPatientNotes.DataSource = dtCombinedNotes;

                descriptor = new GridSortExpression();
                descriptor.FieldName = "PatNum";

                descriptor.SortOrder = GridSortOrder.Ascending;
                RadGridPatientNotes.MasterTableView.AllowCustomSorting = false;
                RadGridPatientNotes.MasterTableView.SortExpressions.Clear();
                RadGridPatientNotes.MasterTableView.SortExpressions.Add(descriptor);

                RadGridPatientNotes.Rebind();
                HighlighGridDiffernces(RadGridPatientNotes, "PatNum");

                DataTable dtCombinedMedication = DSl.GetMedication(m_PatID);
                if (!DSl.IsDataRowEqual(dtCombinedMedication))
                {
                    RadTabStrip1.Tabs[2].ForeColor = Color.Red;
                }
                HighlighGridDiffernces(RadGridPatientMedication, "MedicationPatNum");
                RadGridPatientMedication.DataSource = dtCombinedMedication;

                descriptor = new GridSortExpression();
                descriptor.FieldName = "MedicationPatNum";

                descriptor.SortOrder = GridSortOrder.Ascending;
                RadGridPatientMedication.MasterTableView.AllowCustomSorting = false;
                RadGridPatientMedication.MasterTableView.SortExpressions.Clear();
                RadGridPatientMedication.MasterTableView.SortExpressions.Add(descriptor);

                RadGridPatientMedication.Rebind();
                HighlighGridDiffernces(RadGridPatientMedication, "MedicationPatNum");

                DataTable dtCombinedDisease = DSl.GetDisease(m_PatID);
                if (!DSl.IsDataRowEqual(dtCombinedDisease))
                {
                    RadTabStrip1.Tabs[3].ForeColor = Color.Red;
                }
                HighlighGridDiffernces(RadGridPatientDisease, "DiseaseNum");
                RadGridPatientDisease.DataSource = dtCombinedDisease;

                descriptor = new GridSortExpression();
                descriptor.FieldName = "DiseaseNum";

                descriptor.SortOrder = GridSortOrder.Ascending;
                RadGridPatientDisease.MasterTableView.AllowCustomSorting = false;
                RadGridPatientDisease.MasterTableView.SortExpressions.Clear();
                RadGridPatientDisease.MasterTableView.SortExpressions.Add(descriptor);

                RadGridPatientDisease.Rebind();
                HighlighGridDiffernces(RadGridPatientDisease, "DiseaseNum");


                DataTable dtCombinedAllergy = DSl.GetAllergy(m_PatID);
                if (!DSl.IsDataRowEqual(dtCombinedAllergy))
                {
                    RadTabStrip1.Tabs[4].ForeColor = Color.Red;
                }
                RadGridPatientAllergy.DataSource = dtCombinedAllergy;

                descriptor = new GridSortExpression();
                descriptor.FieldName = "AllergyNum";

                descriptor.SortOrder = GridSortOrder.Ascending;
                RadGridPatientAllergy.MasterTableView.AllowCustomSorting = false;
                RadGridPatientAllergy.MasterTableView.SortExpressions.Clear();
                RadGridPatientAllergy.MasterTableView.SortExpressions.Add(descriptor);

                RadGridPatientAllergy.Rebind();
                HighlighGridDiffernces(RadGridPatientAllergy, "AllergyNum");

                DataTable dtCombinedProcedureLog = DSl.GetProcedureLog(m_PatID);
                if (!DSl.IsDataRowEqual(dtCombinedProcedureLog))
                {
                    RadTabStrip1.Tabs[5].ForeColor = Color.Red;
                }
                RadGridPatientProcedureLog.DataSource = dtCombinedProcedureLog;
                descriptor = new GridSortExpression();
                descriptor.FieldName = "ProcNum";

                descriptor.SortOrder = GridSortOrder.Ascending;
                RadGridPatientProcedureLog.MasterTableView.AllowCustomSorting = false;
                RadGridPatientProcedureLog.MasterTableView.SortExpressions.Clear();
                RadGridPatientProcedureLog.MasterTableView.SortExpressions.Add(descriptor);

                RadGridPatientProcedureLog.Rebind();
                HighlighGridDiffernces(RadGridPatientProcedureLog,"ProcNum");

                DataTable dtCombinedProcedureNote = DSl.GetProcedureNote(m_PatID);
                if (!DSl.IsDataRowEqual(dtCombinedProcedureNote))
                {
                    RadTabStrip1.Tabs[6].ForeColor = Color.Red;
                }
                HighlighGridDiffernces(RadGridPatientProcedureNote, "ProcNoteNum");
                RadGridPatientProcedureNote.DataSource = dtCombinedProcedureNote;


                descriptor = new GridSortExpression();
                descriptor.FieldName = "ProcNoteNum";

                descriptor.SortOrder = GridSortOrder.Ascending;
                RadGridPatientProcedureNote.MasterTableView.AllowCustomSorting = false;
                RadGridPatientProcedureNote.MasterTableView.SortExpressions.Clear();
                RadGridPatientProcedureNote.MasterTableView.SortExpressions.Add(descriptor);

                RadGridPatientProcedureNote.Rebind();
                HighlighGridDiffernces(RadGridPatientProcedureNote, "ProcNoteNum");

                RadGridMain.Visible = true;
                RadTabStrip1.Visible = true;
                lblStudentNotFound.Visible = false;
            }
            else
            {
                RadGridMain.Visible = false;
                RadTabStrip1.Visible = false;
                lblStudentNotFound.Visible = true;
            }
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            

            SqlConnection conn = new SqlConnection(m_strConnectionString);
            SqlCommand cmd = new SqlCommand("[dbo].[ProcessStudent]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            long nPatNum = Convert.ToInt64(Request.QueryString["PatNum"]);
            if (nPatNum > 0)
            {
                cmd.Parameters.AddWithValue("@PatNum", nPatNum);
                cmd.Parameters.AddWithValue("@UserID", 1);
                cmd.Parameters.AddWithValue("@Notes", txtNotes.Text);

                try
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception err)
                {
                    Console.WriteLine("Debug");
                    Console.Write(err);
                }
                finally
                {
                    conn.Close();
                }
                btnProcess.Enabled = false;
                txtNotes.ReadOnly = true;
                btnProcess.Text = "Student has been processed";
            }
        }

        protected void chkShowDiffernces_CheckedChanged(object sender, EventArgs e)
        {
            LoadGrids();
        }
    }
}