﻿<%@ Page Title="Student Lookup" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Student.aspx.cs" Inherits="Condense.About"  EnableEventValidation="false"%>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h2 style="color:#0091ba">Condense - Student Verification Page</h2>
    <br />
    <telerik:RadLabel ID="lblStudentNotFound" Text="Student was not found." Font-Bold="true" Font-Size="X-Large" ForeColor="Red" runat="server" style="padding-left:20px" visible="false"/>
    <telerik:RadLabel ID="lblNotes" Text="Process Notes:" runat="server" />
    <br />
    <telerik:RadTextBox runat="server" ID="txtNotes" Width="1000px" Height="100px" TextMode="MultiLine" Wrap="true"></telerik:RadTextBox>
    <br />
    <telerik:RadButton ID="btnProcess" runat="server" Text="Mark as Processed" OnClick="btnProcess_Click"/>
    <br />
    <br />
    <asp:CheckBox runat="server" ID="chkShowDiffernces" Text="Show Differences Only" Checked="true" OnCheckedChanged="chkShowDiffernces_CheckedChanged" AutoPostBack="true"></asp:CheckBox>
    <br />
<%--    <telerik:RadLabel ID="lblFirstName" Text="First Name:" runat="server" />
    <telerik:RadTextBox ID="txtStudentFirstName" runat="server" Text="" />
    <telerik:RadLabel ID="lblLastName" Text="Last Name:" runat="server" />
    <telerik:RadTextBox ID="txtStudentLastName" runat="server"  Text=""/>
    <telerik:RadLabel ID="lblDOB" Text="DOB (YYYY-MM-DD):" runat="server" />
    <telerik:RadTextBox ID="txtStudentDOB" runat="server"  Text=""/>
    <telerik:RadButton ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click"/>
    <br />
    <br />--%>
    
    <telerik:radgrid ID="RadGridMain" runat="server">
        <MasterTableView AutoGenerateColumns="False" >
            <Columns>
                <telerik:GridBoundColumn DataField="Databasename"  HeaderText="Database Name"></telerik:GridBoundColumn>
            </Columns>
            <Columns>
                <telerik:GridBoundColumn DataField="PatNum" HeaderText="PatNum"></telerik:GridBoundColumn>
            </Columns>
            <Columns>
                <telerik:GridBoundColumn DataField="FName" HeaderText="FName"></telerik:GridBoundColumn>
            </Columns>
            <Columns>
                <telerik:GridBoundColumn DataField="LName" HeaderText="LName"></telerik:GridBoundColumn>
            </Columns>
            <Columns>
                <telerik:GridBoundColumn DataField="PatStatus" HeaderText="PatStatus"></telerik:GridBoundColumn>
            </Columns>
            <Columns>
                <telerik:GridBoundColumn DataField="Birthdate" HeaderText="Birthdate"></telerik:GridBoundColumn>
            </Columns>
            <Columns>
                <telerik:GridBoundColumn DataField="WirelessPhone" HeaderText="WirelessPhone"></telerik:GridBoundColumn>
            </Columns>

            <Columns>
                <telerik:GridBoundColumn DataField="StudentStatus" HeaderText="StudentStatus"></telerik:GridBoundColumn>
            </Columns>
            <Columns>
                <telerik:GridBoundColumn DataField="SchoolName" HeaderText="SchoolName"></telerik:GridBoundColumn>
            </Columns>
            <Columns>
                <telerik:GridBoundColumn DataField="GradeLevel" HeaderText="GradeLevel"></telerik:GridBoundColumn>
            </Columns>
            <Columns>
                <telerik:GridBoundColumn DataField="DateFirstVisit" HeaderText="DateFirstVisit"></telerik:GridBoundColumn>
            </Columns>
            <Columns>
                <telerik:GridBoundColumn DataField="DateTstamp" HeaderText="DateTstamp"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:radgrid>

    <hr />

    <telerik:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip1"  MultiPageID="RadMultiPage1" SelectedIndex="0" Skin="Office2007" >
        <Tabs>
            <telerik:RadTab Text="Patient" Width="175px" Selected="True"></telerik:RadTab>
            <telerik:RadTab Text="Patient Notes" Width="175px"></telerik:RadTab>
            <telerik:RadTab Text="Medication" Width="150px"></telerik:RadTab>
            <telerik:RadTab Text="Disease" Width="160px"></telerik:RadTab>
            <telerik:RadTab Text="Allergy" Width="175px"></telerik:RadTab>
            <telerik:RadTab Text="Procedure Log" Width="225px"></telerik:RadTab>
            <telerik:RadTab Text="Procedure Note" Width="250px"></telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>

    <telerik:RadMultiPage runat="server" ID="RadMultiPage1"  SelectedIndex="0" CssClass="outerMultiPage">
        <telerik:RadPageView runat="server" ID="RadPageView7">
            <telerik:radgrid ID="RadGridPatient" runat="server"></telerik:radgrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView1">
            <telerik:radgrid ID="RadGridPatientNotes" runat="server"></telerik:radgrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView2">
            <telerik:radgrid ID="RadGridPatientMedication" runat="server"></telerik:radgrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView3">
            <telerik:radgrid ID="RadGridPatientDisease" runat="server"></telerik:radgrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView4">
            <telerik:radgrid ID="RadGridPatientAllergy" runat="server"></telerik:radgrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView5">
            <telerik:radgrid ID="RadGridPatientProcedureLog" runat="server"></telerik:radgrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView6">
            <telerik:radgrid ID="RadGridPatientProcedureNote" runat="server"></telerik:radgrid>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Content>
